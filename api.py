from flask import Flask, jsonify, request
from flask_pymongo import PyMongo, MongoClient
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
db = MongoClient('localhost', 27017)
employees = db.techatract.employees


# to initialize the mongodb with the employees, just use this route once.
@app.route('/init')
def init_db():
    init = [
        {
            'name': 'Andreas Harmuth',
            'position': 'CEO',
            'projects': ['Techatract website', 'JIRA admin']
        },
        {
            'name': 'Christian Petersen',
            'position': 'Intern',
            'projects': ['Techatract website']
        },
        {
            'name': 'Sergiu Ojog',
            'position': 'Intern',
            'projects': ['fgt']
        }
    ]

    ids = employees.insert_many(init)
    return 'initialized...'

@app.route('/employees', methods=['GET'])
def get_employees():

    output = []

    for e in employees.find():
        output.append({'name': e['name'], 'position': e['position'], 'projects': e['projects']})

    return jsonify({'results': output})


#change Position to position
@app.route('/employees/<name>', methods=['GET'])
def get_one_employee(name):

    employees.find_one({'name': name})
    e = employees.find_one({'name': name})
    output = {'name': e['name'], 'position': e['position']}

    return jsonify({'results': output})


if __name__ == '__main__':
    app.run(debug=True)